package com.grupo7.hackaton.models;

public class CarModel {
//Añadir año y foto

    private String id;
    private String desc;
    private float price;
    private String color;
    private String foto;

    public CarModel() {
    }

    public CarModel(String id, String desc, float price, String color, String foto) {
        this.id = id;
        this.desc = desc;
        this.price = price;
        this.color = color;
        this.foto = foto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
