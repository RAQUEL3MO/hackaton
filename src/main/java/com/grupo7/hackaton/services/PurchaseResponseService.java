package com.grupo7.hackaton.services;


import com.grupo7.hackaton.models.PurchaseModel;
import org.springframework.http.HttpStatus;


public class PurchaseResponseService {
    private String msg;
    private PurchaseModel purchase;
    private HttpStatus responseHttpStatusCode;

    public PurchaseResponseService(){

    }

    public PurchaseResponseService(String msg, PurchaseModel purchase, HttpStatus responseHttpStatusCode){
        this.msg = msg;
        this.purchase = purchase;
        this.responseHttpStatusCode = responseHttpStatusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PurchaseModel getPurchase() {
        return purchase;
    }

    public void setPurchaseModel(PurchaseModel purchase) {
        this.purchase = purchase;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}
