package com.grupo7.hackaton.services;

import com.grupo7.hackaton.HackatonApplication;
import com.grupo7.hackaton.models.CarModel;
import com.grupo7.hackaton.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@Service
public class CarService {
    @Autowired
    CarRepository carRepository;

    public List<CarModel> findAll() {
        System.out.println("findAll en CarService");

        return this.carRepository.findAll();
    }

    public CarModel add (CarModel car){
      System.out.println("add en CarService");

      return this.carRepository.save(car);
    }
    public Optional<CarModel> findById(String id){
        System.out.println("findById en CarService");

        return  this.carRepository.findById(id);
    }

    public CarModel update(CarModel carModel) {
        System.out.println("update en CarService");

        return this.carRepository.update(carModel);
    }

    public boolean delete(String id) {
        System.out.println("delete en CarService");

        boolean result = false;

        Optional<CarModel> carToDelete = this.findById(id);
        if (carToDelete.isPresent()){

            result = true;
            this.carRepository.delete(carToDelete.get());
        }
        return result;
    }

}






