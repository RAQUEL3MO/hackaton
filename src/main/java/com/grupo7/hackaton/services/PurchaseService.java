package com.grupo7.hackaton.services;



import com.grupo7.hackaton.models.CarModel;
import com.grupo7.hackaton.models.ClientModel;
import com.grupo7.hackaton.models.PurchaseModel;
import com.grupo7.hackaton.repositories.PurchaseRepository;
import com.grupo7.hackaton.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    ClientService clientService;

    @Autowired
    CarService carService;

    //Obtener todas las compras
    public List<PurchaseModel> findAll() {
        System.out.println("findAll en PurchaseService");

        return this.purchaseRepository.findAll();

    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findId en PurchaseService");

        return this.purchaseRepository.findById(id);
    }

    public PurchaseResponseService addPurchase(PurchaseModel purchase) {
        System.out.println("addPurchase en purchaseService");

        PurchaseResponseService result = new PurchaseResponseService();

        result.setPurchaseModel(purchase);


        if (this.clientService.findById(purchase.getUserId()).isPresent() == false) {
            System.out.println("El usuario de la compra no se ha encontrado ");

            result.setMsg("El usuario de la compra no se ha encontrado ");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.findById(purchase.getId()).isPresent() == true) {
            System.out.println("Ya hay una compra con esas id ");

            result.setMsg("Ya hay una compra con esas id ");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;

        }


        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
            if (this.carService.findById(purchaseItem.getKey()).isPresent() == false) {
                System.out.println("El car con la id  " + purchaseItem.getKey() + "no se encuentra en el sistema");
                result.setMsg("El car con la id  " + purchaseItem.getKey() + "no se encuentra en el sistema");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

                return result;
            } else {
                System.out.println("Añadiendo valor del  " + purchaseItem.getValue() + "unidades del car al total ");

                amount += (this.carService.findById(purchaseItem.getKey()).get().getPrice() * purchaseItem.getValue());
            }

        }

        purchase.setAmount(amount);

        // hacer el save
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;

    }
}
