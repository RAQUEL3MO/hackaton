package com.grupo7.hackaton;

import com.grupo7.hackaton.models.CarModel;
import com.grupo7.hackaton.models.ClientModel;
import com.grupo7.hackaton.models.PurchaseModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class HackatonApplication {

	public static ArrayList<CarModel> carModels;
	public static ArrayList<ClientModel> clientModels;
	public static ArrayList<PurchaseModel> purchaseModels;

	public static void main(String[] args) {
		SpringApplication.run(HackatonApplication.class, args);

		HackatonApplication.carModels = HackatonApplication.getCarsData();
		HackatonApplication.clientModels = HackatonApplication.getClientsData();
		HackatonApplication.purchaseModels = HackatonApplication.getPurchasesData();

	}

	private static ArrayList<CarModel> getCarsData(){
	ArrayList<CarModel> carModels = new ArrayList<>();
	carModels.add(
			new CarModel(
					"1"
					,"Hyundai i20"
					,12000
					,"blanco"
					,"./img/car.jpg"
			)
	);
		carModels.add(
				new CarModel(
						"2"
						,"Seat Ibiza"
						,16000
						,"Azul"
						,"./img/car.jpg"
				)
		);
		carModels.add(
				new CarModel(
						"3"
						,"BMW serie 5"
						,45000
						,"Negro"
						,"./img/car.jpg"
				)
		);
		carModels.add(
				new CarModel(
						"4"
						,"Opel Astra"
						,22000
						,"Gris"
						,"./img/car.jpg"
				)
		);
		carModels.add(
				new CarModel(
						"5"
						,"Nissan Qasqai"
						,30000
						,"Verde"
						,"./img/car.jpg"
				)
		);
		carModels.add(
				new CarModel(
						"6"
						,"Peugeot 5008"
						,27000
						,"negro"
						,"./img/car.jpg"
				)
		);

	return carModels;
	}
	private static ArrayList<ClientModel> getClientsData(){
		ArrayList<ClientModel> clientModels = new ArrayList<>();
		clientModels.add(
				new ClientModel(
						"1"
						,"Raquel"
						,47
				)
		);
		clientModels.add(
				new ClientModel(
						"2"
						,"Antonio"
						,32
				)
		);
		clientModels.add(
				new ClientModel(
						"3"
						,"Eugenio"
						,30
				)
		);
		clientModels.add(
				new ClientModel(
						"4"
						,"Sergio"
						,27
				)
		);
		return clientModels;
	}

	private static ArrayList<PurchaseModel> getPurchasesData(){
		return new ArrayList<>();
	}

}
