package com.grupo7.hackaton.controllers;

import com.grupo7.hackaton.models.CarModel;
import com.grupo7.hackaton.models.PurchaseModel;
import com.grupo7.hackaton.models.ClientModel;
import com.grupo7.hackaton.services.ClientService;
import com.grupo7.hackaton.services.PurchaseService;
import com.grupo7.hackaton.services.PurchaseResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hackaton")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})


public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;


    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases(){
        System.out.println("getPurchases");

        return new ResponseEntity<>(
                this.purchaseService.findAll(), HttpStatus.OK
        ) ;
    }

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("La id de la compra a crear es " + purchase.getId());
        System.out.println("El id del usuario a crear es " + purchase.getUserId());
        System.out.println("Los elementos de  la compra  a crear es " + purchase.getPurchaseItems());

        PurchaseResponseService purchaseResponseService = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(
                purchaseResponseService.getPurchase(),
                purchaseResponseService.getResponseHttpStatusCode()

        );
    }

}
